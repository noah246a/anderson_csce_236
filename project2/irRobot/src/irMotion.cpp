/*--------------------------------------------------------------------
Name:   Noah Anderson
Date:   4/20/20
Course: CSCE 236 Embedded Systems (Spring 2020) 
File:   irMotion.cpp
HW/Lab: Project 2, Checkpoint 2

Purp: Allows user to control the robot using an IR remote

Doc:  Project 2 handout, lecture notes, and datasheet
      This program takes inspiration from the test.ino created for
      the previous checkpoint

Academic Integrity Statement: I certify that, while others may have 
assisted me in brain storming, debugging and validating this program, 
the program itself is my own work. I understand that submitting code 
which is the work of other individuals is a violation of the honor   
code.  I also understand that if I knowingly give my original work to 
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/


#include <avr/io.h>
#include <Arduino.h>
#include "motor.h"

#define irPin (1 << 2)
#define irPinValue (PIND & irPin)
#define TRUE 1
#define FALSE 0

//The hex values are true for the elegoo IR remote only
#define forward 0xB946FF00
#define backwards 0xEA15FF00
#define left 0xBB44FF00
#define right 0xBC43FF00
#define stop 0xBA45FF00


volatile uint8_t  newIrPacket = FALSE;
uint16_t count[34];
uint32_t packet, previousPacket;
uint8_t  i, j, speed;

void loop() {
  
    while(irPinValue != 0); //waiting for the IR sensor's signal to go low
    TCNT1 = 0;          
    newIrPacket = FALSE;

    TIFR1 |= (1 << OCF1A);    // Clears Timer/Counter1, Output Compare A Match Flag
    TIMSK1 |= (1 << OCIE1A);    // Enables interrupt on match with OCR1A

    for(i=0; i<34; i++) {

      TCNT1 = 0;                  
      while(irPinValue == 0 && newIrPacket == FALSE); //polling until decoder pin is high

      TCNT1 = 0;                  
      while(irPinValue != 0 && newIrPacket == FALSE);
      count[i] = TCNT1;

    }

    
    j = 0;
    packet = 0xFFFFFFFF;

    //Turning the signal into binary
    for(i=1; i<33; i++){

      //checking for if it's logical 0, if not no action needed since packet is defaulted to all 1s
      if(count[i] >= 130 && count[i] <= 160){
        packet &= ~( (uint32_t) 1 << j);
        j++;
      } else{
        j++;
      }
    
    }
  

    //allow varying of speed by pressing the same button twice
    if(packet == previousPacket){
      speed = 200;
    } else{
      speed = 100;
    }
    if(packet == forward){
      leftMotorForward(speed);
      rightMotorForward(speed);
      previousPacket = packet;
    } 
    else if(packet == backwards){
      leftMotorReverse(speed);
      rightMotorReverse(speed);
      previousPacket = packet;
    } 
    else if(packet == left){
      leftMotorForward(speed);
      rightMotorForward(50);
      previousPacket = packet;
    }
    else if(packet == right){
      leftMotorForward(50);
      rightMotorForward(speed);
      previousPacket = packet;
    }
    else if(packet == stop){
      leftMotorForward(0);
      rightMotorForward(0);
      previousPacket = packet;
    }
    
}

void setup() {

  WDTCSR = 0x00; 

  motorSetup();

  TCCR1A = TCCR1B = 0;
  TCCR1B |= (1 << WGM12) | (1 << CS11) | (1 << CS10);     //CTC with OCR1A top and clock prescaler of 64
  OCR1A = 16250;  //approx. 65 ms
  TCNT1H = 0;  
  TCNT1L = 0;

  //setting pullup resistor
  DDRD &= ~irPin;
  PORTD |= irPin; 
          
}

ISR(TIMER1_COMPA_vect){
  TIMSK1 &= ~(1 << OCIE1A);  // Disables interrupt on match with OCR1A
  newIrPacket = TRUE;
}