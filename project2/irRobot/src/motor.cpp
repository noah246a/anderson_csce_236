/*--------------------------------------------------------------------
Names:  Noah Anderson
Date:   3/30/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   motor.cpp
HW/Lab: Project 1, programming component

Purp: This file contains functions pertaining to the DC motors on the robot

Doc: Lecture slides and datasheet were heavily used

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Arduino.h>

#define leftPWM (1 << 5) //PORTD, OCR0B
#define leftForward (1 << 7) //PORTD
#define leftReverse (1 << 1) //PORTB

#define rightPWM (1 << 6) //PORTD, OCR0A
#define rightForward (1 << 2) //PORTB
#define rightReverse (1 << 3) //PORTB

void motorSetup(){
  //setting output pins
  DDRD |= leftPWM | leftForward | rightPWM;
  DDRB |= leftReverse | rightForward | rightReverse;

  //fast PWM mode: Motor timer 0
  TCCR0A |= (1 << WGM01) | (1 << WGM00) | (1 << COM0B1) | (1 << COM0A1);
  TCCR0B |= (1 << CS00); //1 prescaler
}

void rightMotorForward(uint8_t speed){
  PORTB |= rightForward;
  PORTB &= ~rightReverse;

  OCR0A = speed;
}

void rightMotorReverse(uint8_t speed){
  PORTB &= ~rightForward;
  PORTB |= rightReverse;

  OCR0A = speed;
}

void leftMotorForward(uint8_t speed){
  PORTD |= leftForward;
  PORTB &= ~leftReverse;

  OCR0B = speed;
}

void leftMotorReverse(uint8_t speed){
  PORTD &= ~leftForward;
  PORTB |= leftReverse;

  OCR0B = speed;
}
