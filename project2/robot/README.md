In order to use the libraries ultrasonic.cpp and motor.cpp in conjunction,
You must reserve the following ports for the servo, sensor, motors, etc.:

PD5: PWM left motor
PD6: PWM right motor
PD7: Left forward enable
PB1: Left Reverse enable
PB2: Right forward enable
PB3: Right reverse enable
IO3: Servo
IO4: trigger
IO8: Echo

(optional)
SCK: Controls onboard LED
IO6: Red LED
