/*--------------------------------------------------------------------
Names:  Noah Anderson
Date:   3/30/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   ultrasonic.h
HW/Lab: Project 1, programming component

Purp: The source file contains functions pertaining to the servo/ultrasonic sensor
on the robot.

Doc: Lecture slides and datasheet were heavily used

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#ifndef ULTRASONIC_H_
#define ULTRASONIC_H_
#include <Arduino.h>

/*
 * A quick preface:
 *
 * For this library to work the pins must be mapped as followed:
 * IO3: Servo
 * IO4: trigger
 * IO8: Echo
 * SCK: Controls onboard LED
 * IO6: Red LED
 */

void ultrasonicSetup();

double getDistance();

void analyzeSurroundings();


#endif
