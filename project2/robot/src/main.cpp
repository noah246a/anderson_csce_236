/*--------------------------------------------------------------------
Names:  Noah Anderson
Date:   3/30/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   main.cpp
HW/Lab: Project 1, programming component

Purp: This is the main file for the project, includes the logic for wall following

Doc: Lecture slides and datasheet were heavily used

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Arduino.h>
#include "ultrasonic.h"
#include "motor.h"

#define SENSOR_FRONT 16
#define SENSOR_RIGHT 7

double distanceFromWall, frontDistance;

void setup() {
  ultrasonicSetup();
  motorSetup();
  Serial.begin(9600);
}

//Delays for specified milliseconds using timer 1
//created to clean up my code a bit
void delayMillis(int time){
  for(int i=0; i<time; i++){
    TCNT1 = 0;
    while(TCNT1 < 250);
  }
}

//demonstrates functionality required for project 1 checkpoint 2
void robotMotion(){

  //Forward
  rightMotorForward(255);
  leftMotorForward(255);
  delayMillis(500);
  rightMotorForward(0);
  leftMotorForward(0);
  delayMillis(1000);

  //Backwards
  rightMotorReverse(255);
  leftMotorReverse(255);
  delayMillis(500);
  rightMotorReverse(0);
  leftMotorReverse(0);
  delayMillis(1000);

  //<45 degree left turn
  rightMotorForward(150);
  leftMotorReverse(150);
  delayMillis(250);
  rightMotorForward(0);
  leftMotorReverse(0);
  delayMillis(1000);

  //<45 degree right turn
  rightMotorReverse(150);
  leftMotorForward(150);
  delayMillis(500);
  rightMotorReverse(0);
  leftMotorReverse(0);
  delayMillis(1000);

  //>45 degree left turn
  rightMotorForward(150);
  leftMotorReverse(150);
  delayMillis(750);
  rightMotorForward(0);
  leftMotorReverse(0);
  delayMillis(1000);

  //>45 degree right turn
  rightMotorReverse(150);
  leftMotorForward(150);
  delayMillis(1000);
  rightMotorReverse(0);
  leftMotorReverse(0);
  delayMillis(1000);
}

//Travels within one foot of a wall on the right side of the robot
void wallFollowing(){


  //beginning with forward motion
  rightMotorForward(120);
  leftMotorForward(120);
  OCR2B = SENSOR_RIGHT;
  delayMillis(440);

  distanceFromWall = getDistance();

  //checking for junk values
  while(distanceFromWall > 320){
    leftMotorForward(0);
    rightMotorForward(0);
    distanceFromWall = getDistance();
  }


  //checking if it's reached the end of a wall/obstacle then turning right
  if(distanceFromWall > 25){
    rightMotorForward(0);
    leftMotorForward(150);
    delay(100);
  }

  else if(distanceFromWall < 5){

    //subtle right sweep
    rightMotorForward(140);
    leftMotorForward(0);
    delayMillis(300);
    rightMotorForward(120);
    leftMotorForward(120);
    delayMillis(300);
    rightMotorForward(0);
    leftMotorForward(100);
    delayMillis(300);
  }
  else if(distanceFromWall > 6){

    //subtle left sweep
    rightMotorForward(0);
    leftMotorForward(120);
    delayMillis(300);
    rightMotorForward(120);
    leftMotorForward(150);
    delayMillis(300);
    rightMotorForward(100);
    leftMotorForward(0);
    delayMillis(300);
  }
}

//extension of wall following that checks for any obstacles in the path along the wall
void obstacleAvoidance(){
  OCR2B = SENSOR_FRONT;
  delayMillis(175); //allowing enough time for the servo to adjust

  frontDistance = getDistance();

  if(frontDistance < 20){

    rightMotorForward(140);
    leftMotorForward(0);

  } else{
    wallFollowing();
  }

}

void loop() {

  obstacleAvoidance();
}
