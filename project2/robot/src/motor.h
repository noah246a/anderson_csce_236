/*--------------------------------------------------------------------
Names:  Noah Anderson
Date:   3/30/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   motor.h
HW/Lab: Project 1, programming component

Purp: The source file contains functions pertaining to the DC motors on the robot

Doc: Lecture slides and datasheet were heavily used

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/


#ifndef MOTOR_H_
#define MOTOR_H_
#include <Arduino.h>

//PD5: PWM left motor
//PD6: PWM right motor
//PD7: Left forward enable
//PB1: Left Reverse enable
//PB2: Right forward enable
//PB3: Right reverse enable

// 0 <= speed <= 255


void motorSetup();

void rightMotorForward(uint8_t speed);

void rightMotorReverse(uint8_t speed);

void leftMotorForward(uint8_t speed);

void leftMotorReverse(uint8_t speed);

#endif
