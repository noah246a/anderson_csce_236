/*--------------------------------------------------------------------
Names:   Noah Anderson
Date:   2/13/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   HW_3.cpp
HW/Lab: HW 3, programming component

Purp: This program contains solutions to the coding problems in Homework 3.

Doc: Haley Shaw and I collaborated at a higher level during lab time.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Arduino.h>
#include "morse.h"

#define DOT 0
#define DASH 1
#define RED_LED (1 << 7)
#define GREEN_LED (1 << 6)
#define BLUE_LED (1 << 5)
#define button (1 << 4)

int morseInput[4] = {};
int morseR[] = {DOT, DASH, DOT};
int morseG[] = {DASH, DASH, DOT};
int morseB[] = {DASH, DOT, DOT, DOT};

uint8_t i = 1;

void setup(){
  //setting pins to output
  DDRD |= RED_LED | GREEN_LED | BLUE_LED;
  DDRD &= ~button;

  //Using 1024 prescaler
  TCCR1B &= 0b11111000;
  //Init counter1
  TCCR1A = 0; //Normal mode 0xffff top, rolls over
  TCCR1B |= (1 << CS10) | (1 << CS12);
  TCCR1C = 0;
  //Set counter to zero, high byte first
  TCNT1H = 0;
  TCNT1L = 0;
  //Make sure interrupts are disabled 
  TIMSK1 = 0;
  TIFR1 = 0;

  //Enabling the internal pullup resistor
  MCUCR &= ~(1 << PUD);
  PORTD |= button;

  Serial.begin(9600);
}

//compares to morse letters and returns a 1 or 0 depending on similarity
int compareMorse(int morseLetter[], int morseInput[], int length){
  int result = 1;
  for(i=0; i<length; i++){
    if(morseLetter[i] != morseInput[i]){
      result = 0;    
    }
  }

  return result;
}

void fiveC(){
  morseBlinkChar(GREEN_LED, 'h');
  morseLongPause();
  morseBlinkChar(RED_LED, 'i');
  morseLongPause();
}

void fiveD(){
  //15625 counts = about 1 second
  if((PIND & button) == 0){
    TCNT1 = 0;
    while(TCNT1 < 390); //delaying 25 ms for bounce
    while((PIND & button) == 0);

    if(TCNT1 >= 15625){
      Serial.println("D");
    } else {
      Serial.println("d");
    }

    TCNT1 = 0; //reseting the timer for the pause

  } else if(TCNT1 >= 15625){
    Serial.println("s");
    TCNT1 = 0;
  }
}

void fiveE(){
  //clearing the morse array
  for(i=0; i<4; i++){
    morseInput[i] = -1;
  }

  i=0;
  TCNT1 = 0;

  while((TCNT1 < 15625) && (i<4)){
    if((PIND & button) == 0){
      TCNT1 = 0;
      while(TCNT1 < 390); //bounce delay
      while((PIND & button) == 0);
      
      if(TCNT1 >= 15625){
        morseInput[i] = DASH;
        i++;
      } else if(TCNT1 < 15625) {
        morseInput[i] = DOT;
        i++;
      }
      TCNT1 = 0;
    }
  } 

  if(compareMorse(morseR, morseInput, 3) == 1){
    PORTD |= RED_LED;
    TCNT1 = 0;
    while(TCNT1 < 15625);
    PORTD &= ~RED_LED;
  } else if(compareMorse(morseG, morseInput, 3) == 1){
    PORTD |= GREEN_LED;
    TCNT1 = 0;
    while(TCNT1 < 15625);
    PORTD &= ~GREEN_LED;
  } else if(compareMorse(morseB, morseInput, 4) == 1){
    PORTD |= BLUE_LED;
    TCNT1 = 0;
    while(TCNT1 < 15625);
    PORTD &= ~BLUE_LED;
  }
}

void loop() {
  //fiveC();
  //fiveD();
  fiveE();
}