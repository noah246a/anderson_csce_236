/*
 * CSCE236 Embedded Systems Spring
 * HW4
 */

//These are the functions that we know exist in obfs.cpp

void startOne();
void startTwo();
void startThree();
void startFour();
void setMem(char i);
void runLoop();

int flag = 0, i = 0;

void setup() {              

  startThree();
  startTwo();
  startFour();
  startOne();



  //Write _YOUR_ initials to memory and determine where they are put
  //Note you should only do one at a time since they may overwrite each other
  setMem('n');
  setMem('a');

  while(flag == 0){
    
  }

 //The serial port gets messed up, so this won't print
 Serial.println("Finished setup");

}

void loop() {
  //Figure out how long this function takes to run with
  //and without the button pressed.
  //runLoop();
  
}
