/*--------------------------------------------------------------------
Names:  Noah Anderson
Date:   4/6/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   main.cpp
HW/Lab: lab 4, programming component

Purp: This is the main file for the project, includes the logic for wall following

Doc: Lecture slides and datasheet were heavily used

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Arduino.h>

#define button (1 << 3)
#define LED (1 << 4)

void setup() {
  DDRD |= LED; //setting LED to output
  PORTD |= button; //setting internal pullup resistor

  //enabling INT1 as a falling edge interrupt
  EICRA |= (1 << ISC11);
  EICRA &= ~(1 << ISC10);
  EIMSK |= (1 << INT1);
}

ISR(INT1_vect){
  PORTD |= LED;
}

void loop() {
  if((PIND & button) != 0){
    cli();
    PORTD &= ~LED;
    sei();
  }
}