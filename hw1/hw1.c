/*--------------------------------------------------------------------
Name: 	Noah Anderson
Date: 	1/15/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File: 	hw1.c
HW/Lab:	Homework 1, programming component

Purp:	This program processes my full name.

Doc:	Haley Shaw and I discussed bit operations and string pointers
at a conceptual level.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>

char fullName[] = "Noah Anderson";

/**
 * Return your user name
 **/
char* getFullName(void){
  char *fullNamePtr = fullName;
  return fullNamePtr;
}

/**
 *returns last name
 **/
char* getLastName(void){
  char *lastNamePointer = &fullName[5];
  return lastNamePointer;
}

/**
 * Gets the ith character of the user name or returns '!' if the name
 * is not that long.
 **/
char getFullNameChar(int i){
  char *name = getFullName();
  int length=0;

  //obtaining length
  while(name[length+1] != '\0'){
    length++;
  }

  if(i>length){
    return '!';
  } else {
    return name[i];
  }
}

/**
 * Print the results of the string operation tests
 **/
void stringOperations(void){
  char *name = getFullName();

  printf("Name: %s\n",name);
  printf("LastName: %s\n",getLastName());
  printf("Name[0]: %c\n",getFullNameChar(0));
  printf("Name[4]: %c\n",getFullNameChar(4));
  printf("Name[%d] (last): %c\n",(int)strlen(name)-1,
         getFullNameChar(strlen(name)-1));
  printf("Name[%d] (oob): %c\n",(int)strlen(name),
         getFullNameChar(strlen(name)));
  printf("Name[%d] (oob): %c\n",(int)strlen(name)+10,
         getFullNameChar(strlen(name)+10));

  printf("\n");
}

/**
 * Print the output of various bit operations
 **/
void bitOperations(void){
  int varA = 0x81914;
  int varB = 0x10236;

  // Output the value (1 or 0) of bit 3 (zero referenced) in varA and varB
  printf("Bit 3 in varA: %#x\n",
   (varA >> 2) & 0x00001);
  printf("Bit 3 in varB: %#x\n",
   (varB >> 2) & 0x00001);

  // Output the value of varA after setting bit 5 (zero referenced) to 0
  printf("Bit 5 set to 0 varA: %#x\n",
           varA & 0xFFFEF);

  // Output the value of varB after setting bit 7 (zero referenced) to 1
  printf("Bit 7 set to 1 varB: %#x\n",
           varB | 0x40);

  // Output the value of the varA after setting the 3-5 bits
  // (inclusive) of varA to the lower 3 bits of varB.
  printf("varA after...: %#x\n",
           (varA & 0xFFFE3) | ((varB << 2) & 0x1C));

}

/**
 * Main function that calls the corresponding functions
 **/
int main(){
  printf("CSCE 236 HW1 Output\n");
  printf("-------------------\n");
  stringOperations();
  bitOperations();

  return(0);
}
