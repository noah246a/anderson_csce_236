/*--------------------------------------------------------------------
Name:   Noah Anderson
Date:   1/20/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   blink.ino
HW/Lab: Homework 1, programming component

Purp: This program demonstrates the built in LED on the arduino

Doc: This program is based on the blink program in the built in examples

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

int i;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop(){
  for(i=0;i<5;i++){
  digitalWrite(LED_BUILTIN, HIGH); //long on
  delay(2000);
     
  digitalWrite(LED_BUILTIN, LOW);  //short off
  delay(300);
  
  digitalWrite(LED_BUILTIN, HIGH); //short on
  delay(300);
  
  digitalWrite(LED_BUILTIN, LOW);  //short off     
  delay(300);
  
  digitalWrite(LED_BUILTIN, HIGH); //short on
  delay(300); 
  
  digitalWrite(LED_BUILTIN, LOW);  //long off
  delay(2000);
  
  digitalWrite(LED_BUILTIN, HIGH); //long on
  delay(2000);
  
  digitalWrite(LED_BUILTIN, LOW); //long off
  delay(2000);
  }
  return;
}
