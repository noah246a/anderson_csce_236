/*--------------------------------------------------------------------
Name:   Noah Anderson
Date:   1/15/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   hw1.c
HW/Lab: Homework 1, programming component

Purp: Based on the button's state, the speed of the blinking LED changes

Doc:  Heavily inspired by the button example built in to the ide

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

const int buttonPin = 5;     
const int ledPin =  13;


int buttonState = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT);
}

void loop() {

  buttonState = digitalRead(buttonPin);

  //if buttonState is low, the button is pressed, otherwise the button is released
  if (buttonState == HIGH) {
    //short on/off
    digitalWrite(ledPin, HIGH);
    delay(300);
    digitalWrite(ledPin,LOW);
    delay(300);
  } else {
    //long on/off
    digitalWrite(ledPin, HIGH);
    delay(2000);
    digitalWrite(ledPin, LOW);
    delay(2000);
  }
}
