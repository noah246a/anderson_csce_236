#include <Arduino.h>
#include "ultrasonic.h"

void setup() {

  ultrasonicSetup();

  Serial.begin(9600);
}

void servoTest(){
  OCR2B = 6;
  delay(1000);
  OCR2B = 12;
  delay(1000);
  OCR2B = 18;
  delay(1000);
}

void ultrasonicTest(){
  Serial.println(getDistance());
  delay(750);
}

void loop() {
  //servoTest();
  ultrasonicTest();
  //analyzeSurroundings();  
}

