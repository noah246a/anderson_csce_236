#ifndef ULTRASONIC_H_
#define ULTRASONIC_H_
#include <Arduino.h>

/*
 * A quick preface:
 * 
 * For this library to work the pins must be mapped as followed:
 * IO3: Servo
 * IO4: trigger
 * IO8: Echo
 * SCK: Controls onboard LED 
 * IO6: Red LED
 */

void ultrasonicSetup();

//returns distance from ultrasonic sensor in inches
double getDistance();

/*keep ledPin confined to PORTD
 *As the servo pans the ultrasonic sensor
 *checks for an obstruction <1 foot away
 *LEDs are activated according to whether the obstacle
 *is in front, right, or left.
 */
void analyzeSurroundings();

  
#endif
