#include <Arduino.h>

#define echo (1 << 0) //IO8
#define servo (1 << 3) //I03
#define trig (1 << 4) //I04
#define onboardLED (1 << 5) //SCK
#define redLED (1 << 6) //I06

double distance;

void ultrasonicSetup(){

  //setting output pins
  DDRD |= servo | trig | redLED;
  DDRB |= onboardLED;

  // Phase correct mode PWM: Servo timer
  TCCR2A = 1 << COM2B1 | 1 << WGM20;
  TCCR2B = 1 << WGM22 | 1 << CS22 | 1 << CS21 | 1 << CS20; //prescaler 1024
  OCR2A = 156;

  //Regular 16 bit timer with a prescaler of 64
  TCCR1A = 0;
  TCCR1B |= (1 << CS10) | (1 << CS11);
}

double getDistance(){

  //trigger pulse
  PORTD |= trig;
  TCNT1 = 0;
  while(TCNT1 < 3); //delaying a little over 10 us
  PORTD &= ~trig;

  //waiting for echo to go high
  while((PINB & echo) == 0);

  TCNT1 = 0;
  ICR1 = 0;
  while(TCNT1 < 500); //datasheet suggests waiting 2 ms for a signal to return

  return (double) (ICR1*4)/148; //converting counts to inches
}

void analyzeSurroundings(){

  //sweeping from -90 degrees to 90
  for(OCR2B = 6; OCR2B <= 18; OCR2B += 2){

    TCNT1 = 0;
    while(TCNT1 < 15000); //delaying 60 ms

    distance = getDistance();
    if(distance > 0 && distance <= 12){
      if(OCR2B <= 12){
        PORTD |= redLED;
      }
      if(OCR2B >= 12){
        PORTB |= onboardLED;
      }
    }
  }

  //sweeping from 90 degrees to -90
  for(OCR2B = 18; OCR2B >= 6; OCR2B -= 2){

    TCNT1 = 0;
    while(TCNT1 < 15000); //delaying 60 ms

    distance = getDistance();
    if(distance > 0 && distance <= 12){
      if(OCR2B <= 12){
        PORTD |= redLED;
      }
      if(OCR2B >= 12){
        PORTB |= onboardLED;
      }
    }
  }
  PORTB &= ~onboardLED;
  PORTD &= ~redLED;
}
