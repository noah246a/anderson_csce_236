#include <Arduino.h>

#define red 5
#define blue 7
#define green 6

void setup() {
  DDRD |= (1 << red); //setting red LED output
  DDRD |= (1 << blue); //setting blue LED output
  DDRD |= (1 << green); //setting green LED output
}

void loop(){
  if(PORTD & (1 << 4)){
    PORTD |= (1 << red); //R
    delay(1000);
    PORTD |= (1 << green); //RG
    delay(1000);
    PORTD |= (0 << red); //G
    delay(1000);
    PORTD |= (1 << blue); //GB
    delay(1000);
    PORTD |= (0 << green); //B
    delay(1000);
    PORTD |= (1 << red); //RB
    delay(1000);
    PORTD |= (0 << red); //off
    PORTD |= (0 << blue);
    delay(1000);
  }
}