/*--------------------------------------------------------------------
Names:  Noah Anderson and Haley Shaw
Date:   1/24/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   lightTrackingV2.ino
HW/Lab: Lab 1

Purp: The purpose of program is to track light using a single photoresistor

Doc:  none

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Servo.h>

int pos=90, lightA, lightB;
Servo servo;

void setup() {
  servo.attach(9); //servo pin
}

void loop() {
  //keep the servo within bounds
  if(pos >= 180) {
    pos = 170;
  }
  if(pos <= 0) {
    pos = 10;
  }

  //move servo so photoresistors can check surroundings for a brighter
  //source of light and records the left and right surroundings
  servo.write(pos+10);
  delay(150);
  lightA = analogRead(A0);
  servo.write(pos-20);
  delay(150);
  lightB = analogRead(A0);

  //if the photoresister detects more light at one of the positioins
  //above, move it to that position
  if(lightA > lightB){
    pos = pos+10;
    servo.write(pos);
    delay(150);
  } else{
    pos = pos-20;
    servo.write(pos);
    delay(150);
  }
}
