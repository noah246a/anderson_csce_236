/*--------------------------------------------------------------------
Names:   Noah Anderson, Haley Shaw
Date:   1/24/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   servo.ino
HW/Lab: Lab 1

Purp: This script is designed to demonstrate some capabilities of a servo

Doc:  I used the Arduino website to find documentation for the Servo.h library

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Servo.h>

Servo servo;
int pos, buttonValue;

void setup() {
  servo.attach(9); //servo pin
  servo.write(0); //setting home position
  delay(500);
}

void loop() {
    servo.write(90);
    delay(1000);
    servo.write(180);
    delay(1000);
    servo.write(0);
    delay(1000);
}
