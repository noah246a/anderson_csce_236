/*--------------------------------------------------------------------
Names:  Noah Anderson and Haley Shaw
Date:   1/24/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   lab_1.ino
HW/Lab: Lab 1

Purp: This program contains solutions to lab_1.

Doc:  I used the Arduino website to find documentation for the Servo.h library

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Servo.h>

#define redPin (1 << 7)
#define bluePin (1 << 6)
#define greenPin (1 << 5)
#define buttonPin (1 << 4)

Servo servo;
int result, pos, photoResisA, photoResisB;

void setup() {
  servo.attach(9); //for servo and lightTrackingV1+V2
  DDRD |= redPin; //setting red LED output
  DDRD |= bluePin; //setting blue LED output
  DDRD |= greenPin; //setting green LED output
  Serial.begin(9600);
  
}

void loop() {
  //servoTest();
  //lightTrackingV1();
  //lightTrackingV2();
  //rgb();
  //photoresistor();
}

void servoTest() {
  if((PIND & buttonPin) == 0){ //when button is pressed
    servo.write(90);
    delay(1000);
    servo.write(180);
    delay(1000);
    servo.write(0);
    delay(1000);
  }
}

void lightTrackingV1() {
  photoResisA = analogRead(A0);
  photoResisB = analogRead(A1);

  //moving the servo arm in the direction of the photoresistor that's recieving more light
  //until both photoresistors are recieving the same amount of light
  if((photoResisA > photoResisB) && (pos < 180)){
    pos++;
    servo.write(pos);
    delay(25);
  } else if((photoResisB > photoResisA) && (pos > 0)){
    pos -= 1;
    servo.write(pos);
    delay(25);
  }
}

void lightTrackingV2(){
  //keep the servo within bounds
  if(pos >= 180) {
    pos = 170;
  }
  if(pos <= 0) {
    pos = 10;
  }

  //move servo so photoresistors can check surroundings for a brighter
  //source of light and records the left and right surroundings
  servo.write(pos+10);
  delay(150);
  photoResisA = analogRead(A0);
  servo.write(pos-20);
  delay(150);
  photoResisB = analogRead(A0);

  //if the photoresister detects more light at one of the positioins
  //above, move it to that position
  if(photoResisA > photoResisB){
    pos = pos+10;
    servo.write(pos);
    delay(150);
  } else if(photoResisB > photoResisA){
    pos = pos-20;
    servo.write(pos);
    delay(150);
  }
}

void rgb(){
  if((PIND & buttonPin) == 0){
    PORTD |= redPin; //R
    delay(1000);
    PORTD |= greenPin; //RG
    delay(1000);
    PORTD &= ~redPin; //G
    delay(1000);
    PORTD |= bluePin; //GB
    delay(1000);
    PORTD &= ~greenPin; //B
    delay(1000);
    PORTD |= redPin; //RB
    delay(1000);
    PORTD &= ~redPin; //off
    PORTD &= ~bluePin;
    delay(1000);
  }
}

void photoresistor(){
  result = analogRead(A0);
  Serial.print(result);
  Serial.print("\n");
  delay(500);
}
