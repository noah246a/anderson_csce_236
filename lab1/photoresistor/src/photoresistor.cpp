/*--------------------------------------------------------------------
Names: 	Noah Anderson and Haley Shaw
Date: 	1/24/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File: 	photoresistor.cpp
HW/Lab:	Lab 1

Purp:	The purpose of this program is to test a photoresistor

Doc:	none

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Arduino.h>

int result;

void setup() {
  Serial.begin(9600);
}

void loop() {
  result = analogRead(A0);
  Serial.print(result);
  Serial.print("\n");
  delay(500);
}