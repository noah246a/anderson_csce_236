/*--------------------------------------------------------------------
Names:  Noah Anderson and Haley Shaw
Date:   1/24/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   lightTrackingV1.ino
HW/Lab: Lab 1

Purp: This purpose of this program is to track the strongest light source using
2 photoresistors and a servo.

Doc:  I used the Arduino website to find documentation for the Servo.h library

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Servo.h>

//photoResisA => A0, photoResisB => A1
int photoResisA, photoResisB, pos;
Servo servo;

void setup() {
  servo.attach(9);
}

void loop() {
  photoResisA = analogRead(A0);
  photoResisB = analogRead(A1);

  //moving the servo arm in the direction of the photoresistor that's recieving more light
  //until both photoresistors are recieving the same amount of light
  if((photoResisA > photoResisB) && (pos < 180)){
    pos++;
    servo.write(pos);
    delay(25);
  } else if((photoResisB > photoResisA) && (pos > 0)){
    pos -= 1;
    servo.write(pos);
    delay(25);
  }
}
