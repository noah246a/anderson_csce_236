/*--------------------------------------------------------------------
Names: 	Noah Anderson, Haley Shaw
Date: 	1/24/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File: 	rgb.cpp
HW/Lab: Lab 1

Purp:	This program tests the abilities of an RGB light

Doc:	Lecture slides and the arduino schematic were referenced

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Arduino.h>

#define redPin (1 << 10)
#define bluePin (1 << 8)
#define greenPin (1 << 9)

void setup() {
  DDRD |= redPin; //setting red LED output
  DDRD |= bluePin; //setting blue LED output
  DDRD |= greenPin; //setting green LED output
}

void loop() {
    PORTD |= redPin; //R
    delay(1000);
    PORTD |= greenPin; //RG
    delay(1000);
    PORTD &= ~redPin; //G
    delay(1000);
    PORTD |= bluePin; //GB
    delay(1000);
    PORTD &= ~greenPin; //B
    delay(1000);
    PORTD |= redPin; //RB
    delay(1000);
    PORTD &= ~redPin; //off
    PORTD &= ~bluePin;
    delay(1000);
}