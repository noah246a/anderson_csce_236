/*--------------------------------------------------------------------
Names:   Noah Anderson and Haley Shaw
Date:   2/6/2020
Course: CSCE 236 Embedded Systems (Spring 2020)
File:   lab2.cpp
HW/Lab: lab 2, programming component

Purp: This program contains solutions to the coding problems in lab 2.

Doc: Haley Shaw and I collaborated at a higher level during lab time.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Arduino.h>

#define led (1 << 5)

int8_t serialInput, i;

uint8_t cntr8 = 0xff, nested8 = 0xff, twiceNested8 = 0xff;
uint16_t cntr16 = 0xffff, nested16 = 0xffff;
uint32_t cntr32 = 0xffffffff;

int delayTime = 250;

void twoA();
void threeB();

void setup() {
  DDRB |= led;
  Serial.begin(9600);
}

void loop() {
  //twoA();
  threeB();
}

void twoA(){
  serialInput = Serial.read();

  if(serialInput == 0x66){ //checking for 'f'
    delayTime = 250;
  } else if(serialInput == 0x73){ //checking for 's'
    delayTime = 1000;
  } else{
    for(i=0; i<serialInput-0x30; i++){
      PORTB |= led;
      delay(delayTime);
      PORTB &= ~led;
      delay(delayTime);
    }
  }
}

void threeB(){

  //6 second delay
  PORTB |= led;
  for(cntr32=0; cntr32<7338260; cntr32++){
        asm volatile("nop");
  }

  //blink
  PORTB &= ~led;
  delay(500);
  PORTB |= led;
  delay(500);
  PORTB &= ~led;
  delay(500);

  //3 second delay
  PORTB |= led;
  for(cntr16=0; cntr16<65525; cntr16++){
    for(nested16=0; nested16<119; nested16++){
        asm volatile("nop");
    }
  }

  //2xblink
  PORTB &= ~led;
  delay(500);
  PORTB |= led;
  delay(500);
  PORTB &= ~led;
  delay(500);
  PORTB |= led;
  delay(500);
  PORTB &= ~led;
  delay(500);

  //2 second delay
  PORTB |= led;
  for(cntr8=0; cntr8<255; cntr8++){
    for(nested8=0; nested8<255; nested8++){
      for(twiceNested8=0; twiceNested8<96; twiceNested8++){
        asm volatile("nop");
      }
    }
  }

  PORTB &= ~led;
  delay(2000);

}
