


#include <Arduino.h>
//#include <avr/io.h>

// Definitions for Port B & D registors
#define PINB_Reg (*((volatile uint8_t *) 0x23))
#define DDRB_Reg (*((volatile uint8_t *) 0x24))
#define PORTB_Reg (*((volatile uint8_t *) 0x25))
#define PIND_Reg (*((volatile uint8_t *) 0x29))
#define DDRD_Reg (*((volatile uint8_t *) 0x2a))
#define PORTD_Reg (*((volatile uint8_t *) 0x2b))
// Definitions for LED assignments:
#define BOARD_LED 5   //pin 13 is PortB bit 5
#define RED_LED 1     //pin 9 is PortB bit 1  
#define GREEN_LED 2  //pin 10 is PortB bit 2
#define BLUE_LED 3   //pin 11 is PortB bit 3

#define BUTTON 5 

unsigned long wastedMicros, measuredMicros, buttonCount = 0;
uint8_t tempReg = TCNT1;


/**
 * Init all of the LEDs and test them
 **/ 
void LEDInit(){
 //Set pinmode for LEDs to output 
  DDRB_Reg |= (1 << BOARD_LED);
  DDRB_Reg |= (1 << RED_LED);
  DDRB_Reg |= (1 << GREEN_LED);
  DDRB_Reg |= (1 << BLUE_LED);

  //Turn all off
  PORTB_Reg &= ~(1 << BOARD_LED); //clear output
  PORTB_Reg &= ~(1 << RED_LED);   //clear output
  PORTB_Reg &= ~(1 << GREEN_LED); //clear output
  PORTB_Reg &= ~(1 << BLUE_LED);  //clear output

  //Test LEDs
  Serial.println("Testing LEDs...");
  PORTB_Reg |= (1 << BOARD_LED);  //set output
  PORTB_Reg |= (1 << RED_LED);    //set output
  delay(400);
  PORTB_Reg &= ~(1 << RED_LED);   //clear output
  PORTB_Reg |= (1 << GREEN_LED);  //set output
  delay(400);
  PORTB_Reg &= ~(1 << GREEN_LED); //clear output
  PORTB_Reg |= (1 << BLUE_LED);   //set output
  delay(400);
  PORTB_Reg &= ~(1 << BLUE_LED);   //clear output
  PORTB_Reg &= ~(1 << BOARD_LED);   //clear output
  Serial.println("Finished LED testing!");
}

void setup() {                
  Serial.begin(9600);
  Serial.println("Starting up...");
  LEDInit();
  //Set pinmode for Button as input
  DDRD_Reg &= ~(1 << BUTTON);
  //Enable pullup 
  PORTD_Reg |= (1 << BUTTON);  //set output to enable pullup resistor
  pinMode(8, INPUT);


  TCCR1B &= 0b11111000;
  //Init counter1
  TCCR1A = 0; //Normal mode 0xffff top, rolls over
  TCCR1B |= (1 << CS10) | (1 << CS11);
  TCCR1C = 0;
  //Set counter to zero, high byte first
  TCNT1H = 0;
  TCNT1L = 0;  
  //Make sure interrupts are disabled 
  TIMSK1 = 0;
  TIFR1 = 0;
  
  Serial.println("Finished setup!");
}

void twoPointTwo(){
  if(tempReg != TCNT1){
    Serial.print("TCNT1: ");
    Serial.println(TCNT1);
  }

  if((TCNT1 - tempReg) > 1){
    Serial.print("Warning: bounce!\n");
  }

  tempReg = TCNT1;
  delay(100);
}

void twoPointThree() {
  //Scaler = 1
  TCCR1B &= 0b11111000;
  TCCR1B |= (1 << CS10);
  TCNT1= 0;
  wastedMicros = micros();
  while(TCNT1 != 0xffff);
  measuredMicros = micros() - wastedMicros;
  Serial.print("Scaler 1 = ");
  Serial.print(measuredMicros);
  Serial.print(" micro seconds\n");

  //Scaler = 8
  TCCR1B &= 0b11111000;
  TCCR1B |= (1 << CS11);
  TCNT1= 0;
  wastedMicros = micros();
  while(TCNT1 != 0xffff);
  measuredMicros = micros() - wastedMicros;
  Serial.print("Scaler 8 = ");
  Serial.print(measuredMicros);
  Serial.print(" micro seconds\n");

  //Scaler = 64
  TCCR1B &= 0b11111000;
  TCCR1B |= (1 << CS10) | (1 << CS11);
  TCNT1= 0;
  wastedMicros = micros();
  while(TCNT1 != 0xffff);
  measuredMicros = micros() - wastedMicros;
  Serial.print("Scaler 64 = ");
  Serial.print(measuredMicros);
  Serial.print(" micro seconds\n");

  //Scaler = 256
  TCCR1B &= 0b11111000;
  TCCR1B |= (1 << CS12);
  TCNT1= 0;
  wastedMicros = micros();
  while(TCNT1 != 0xffff);
  measuredMicros = micros() - wastedMicros;
  Serial.print("Scaler 256 = ");
  Serial.print(measuredMicros);
  Serial.print(" micro seconds\n");

  //Scaler 1024
  TCCR1B &= 0b11111000;
  TCCR1B |= (1 << CS10) | (1 << CS12);
  TCNT1= 0;
  wastedMicros = micros();
  while(TCNT1 != 0xffff);
  measuredMicros = micros() - wastedMicros;
  Serial.print("Scaler 1024 = ");
  Serial.print(measuredMicros);
  Serial.print(" micro seconds\n");
}

void twoPointFour() {
  if(digitalRead(8) == 0){
    buttonCount++;
    TCNT1 = 0;
    ICR1 = 0;

    while(TCNT1 != 0x61A8); // approx. 100 mselec

    if(ICR1 > 0){
      Serial.print("Button bounced for ");
      Serial.print(((long) ICR1)*4);
      Serial.print(" micro seconds\n");
    }

    while(digitalRead(8) != 1);
    Serial.print("Button has been pressed ");
    Serial.print(buttonCount);
    Serial.print(" times\n");
  }
}

void twoPointFive(){

  if(digitalRead(8) == 0){
    buttonCount++;

    while(digitalRead(8) != 1);

    TCNT1 = 0;
    ICR1 = 0;

    while(TCNT1 != 0x61A8); // approx. 100 ms

    if(ICR1 > 0){
      Serial.print("Button bounced for ");
      Serial.print(((long) ICR1)*4);
      Serial.print(" micro seconds after released\n");
    }

    Serial.print("Button has been pressed ");
    Serial.print(buttonCount);
    Serial.print(" times\n");
  }
}

void twoPointSix(){
  
  TCCR1B &= 0b11111000;
  TCCR1B |= (1 << CS10) | (1 << CS12);

  if(digitalRead(8) == 0){
    TCNT1 = 0;
    
    while(TCNT1 < 469); //delaying for 30 milliseconds
    while(digitalRead(8) == 0);

    Serial.print("You held the button for ");
    Serial.print(((long) TCNT1)*64);
    Serial.print(" microseconds\n");

    TCNT1 = 0;
    while(TCNT1 < 469); //delaying for 30 milliseconds

  }
}

void loop() {
  //twoPointThree();
  //twoPointFour();
  //twoPointFive();
  twoPointSix();
}

